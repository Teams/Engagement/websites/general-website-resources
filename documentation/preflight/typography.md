---
layout: default
title: Typography
parent: Preflight
nav_order: 2
permalink: /preflight/typography
---

# Typography

<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<p>Body Text</p>

~~~html
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<p>Body Text</p>
~~~

# Text Styles
## Italics

<i>The brown fox jumped over the lazy dog.</i>

~~~html
<i>The brown fox jumped over the lazy dog.</i>
~~~

## Bold

<b>The brown fox jumped over the lazy dog.</b>

~~~html
<b>The brown fox jumped over the lazy dog.</b>
~~~

## Emphasis

<p>The brown fox <em>jumped</em> over the lazy dog.</p>

~~~html
<p>The brown fox <em>jumped</em> over the lazy dog.</p>
~~~

## Mark text

<p>The brown fox <mark>jumped</mark> over the lazy dog.</p>

~~~html
<p>The brown fox <mark>jumped</mark> over the lazy dog.</p>
~~~

## Strikethrough

<s>The brown fox jumped over the lazy dog.</s>

~~~html
<s>The brown fox jumped over the lazy dog.</s>
~~~

## Small side comment

<small>The brown fox jumped over the lazy dog.</small>

~~~html
<small>The brown fox jumped over the lazy dog.</small>
~~~

## Subscript element

<p>Almost every developer's favorite molecule is
C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub>, also known as "caffeine."</p>

~~~html
<p>Almost every developer's favorite molecule is
C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub>, also known as "caffeine."</p>
~~~

## Superscript element 

<p>The <em>Pythagorean theorem</em> is often expressed as the following equation:</p>

<p><var>a<sup>2</sup></var> + <var>b<sup>2</sup></var> = <var>c<sup>2</sup></var></p>

~~~html
<p>The <em>Pythagorean theorem</em> is often expressed as the following equation:</p>
<p><var>a<sup>2</sup></var> + <var>b<sup>2</sup></var> = <var>c<sup>2</sup></var></p>
~~~

## (Date) Time element 

<p>GNOME celebrated their 25th anniversary on <time datetime="2022-08-15">August 15</time> in Guadalajara, Mexico.
The conference starts at <time datetime="09:00"> 9:00 am.</time></p>

~~~html
<p>GNOME celebrated their 25th anniversary on <time datetime="2022-08-15">August 15</time> in Guadalajara, Mexico.
The conference starts at <time datetime="09:00"> 9:00 am.</time></p>
~~~

## The Abbreviation element

<p><abbr title="GNU Network Object Model Environment">GNOME</abbr> was started on 15 August 1997 by Miguel de Icaza and Federico Mena as a free software project to develop a desktop environment and applications for it.</p>

~~~html
<p><abbr title="GNU Network Object Model Environment">GNOME</abbr> was started on 15 August 1997 by Miguel de Icaza and Federico Mena as a free software project to develop a desktop environment and applications for it.</p>
~~~

## The Article Contents element

<article>
  <h3>GNOME Circle</h3>
  <p>GNOME Circle champions the great software that is available for the GNOME platform. Not only do we showcase the best apps and libraries for GNOME, but we also support independent developers who are using GNOME technologies.</p>
</article>

~~~html
<article>
  <h3>GNOME Circle</h3>
  <p>GNOME Circle champions the great software that is available for the GNOME platform. Not only do we showcase the best apps and libraries for GNOME, but we also support independent developers who are using GNOME technologies.</p>
</article>
~~~