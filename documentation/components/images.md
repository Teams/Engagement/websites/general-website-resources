---
layout: default
title: Images
parent: Components
nav_order: 5
permalink: /components/images
---

# Images

## Squared Image
<div>
    <img src= "https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" alt="The GNOME Logo">
</div>

~~~html
<div>
    <img src="https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" alt="The GNOME Logo">
</div>
~~~

## Rounded Corners 
<div class="container">
    <img src="https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" class="round-corners"
     alt="The GNOME Logo">
</div>
~~~html
<div class="container">
    <img src="https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" class="round-corners"
     alt="The GNOME Logo">
</div>
~~~

## Circle Image 
<div class="container">
    <img src="https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" class="circle-image"
     alt="The GNOME Logo">
</div>
~~~html
<div class="container">
    <img src="https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" class="circle-image"
     alt="The GNOME Logo">
</div>
~~~

## Hover Effects
<div class="container">
    <img src="https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" class="hover:scale-105" alt="The GNOME Logo">
</div>
~~~html
<div class="container">
    <img src="https://media-exp1.licdn.com/dms/image/C560BAQE4IgSJqTsoQg/company-logo_200_200/0/1582757051905?e=2147483647&v=beta&t=WbtV_w-20z8o_N9dMZ6GAb7gEruJwvt5t_LYKkN5E-E" class="hover:scale-105" alt="The GNOME Logo">
</div>
~~~