---
layout: default
title: Links
parent: Components
nav_order: 5
permalink: /components/links
---

# Links
## Normal link

<br>
<div class="container">
    <a class="with-underline" href="https://www.gnome.org/"> GNOME Main Website</a> 
</div>
<br>

~~~html
<div class="container">
    <a class="with-underline" href="https://www.gnome.org/"> GNOME Main Website</a> 
</div>
~~~

## No Underline

<br>
<div class="container">
    <a class="sans-underline" href="https://www.gnome.org/"> GNOME Main Website</a> 
</div>
<br>

~~~html
<div class="container">
    <a class="sans-underline" href="https://www.gnome.org/"> GNOME Main Website</a> 
</div>
~~~
