const postcssPresetEnv = require('postcss-preset-env');

module.exports = {
  plugins: [
    require('postcss-omit-import-tilde'),
    require('postcss-import'),
    require('postcss-nested'),
    require('tailwindcss/nesting'),
    require('tailwindcss'),
    require('autoprefixer'),
    postcssPresetEnv({
      browsers: 'chrome >= 50'
    })
  ],
};
